/*
 * beetlejuice
 * beetlejuice-cdm
 * Copyright (C) 2011-2012 art of coding UG, http://www.art-of-coding.eu/
 *
 * Alle Rechte vorbehalten. Nutzung unterliegt Lizenzbedingungen.
 * All rights reserved. Use is subject to license terms.
 *
 * rbe, 29.11.12 13:51
 */

package eu.artofcoding.beetlejuice.cdm.accounting;

public enum BankAccountType {

    NONE, CREDITNOTE, BANKACCOUNT, PAYPAL, CREDITCARD, DEBITCARD

}
