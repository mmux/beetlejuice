/*
 * beetlejuice
 * beetlejuice-cdm
 * Copyright (C) 2011-2012 art of coding UG, http://www.art-of-coding.eu/
 *
 * Alle Rechte vorbehalten. Nutzung unterliegt Lizenzbedingungen.
 * All rights reserved. Use is subject to license terms.
 *
 * rbe, 12.12.12 17:18
 */

package eu.artofcoding.beetlejuice.cdm;

import eu.artofcoding.beetlejuice.cdm.accounting.BankAccount;

public class Company extends Base {

    private String companyIdent;

    private PostalAddress postalAddress;

    private BankAccount bankAccount;

}
